﻿/*
 * Program:         Matching
 * Module:          MainWindow.xaml.cs
 * Date:            April 2nd, 2019
 * Author:          Jinzhi Yang 
 * Description:     The code-behind for the main (and only) window run in the 
 *                  Project 2 executable.
 *                  
 *                  Contains client-side logic to update GUI based on server calls.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MatchingLibrary;

using System.ServiceModel;  // WCF types
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;

namespace Project_2___Matching
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, ICallback
    {

        #region clientProperties
        List<Button> cards;
        GameBoard gameboard;
        Uri back = new Uri("Images/0.jpg", UriKind.Relative);
        private string playerName = "";
        private string currentPlayer = "Player1";
        private List<string> playerNames;
        private int clickCredit = 2;
        public string c1 { get; set; }
        public string c2 { get; set; }
        private IGameBoard gameServer = null;
        public ObservableCollection<Card> cardList;
        public List<ListBoxItem> playerScores;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            gameboard = new GameBoard();

            //this.DataContext = DataContext;

            cards = new List<Button>
            {
                card1,
                card2,
                card3,
                card4,
                card5,
                card6,
                card7,
                card8,
                card9,
                card10,
                card11,
                card12,
                card13,
                card14,
                card15,
                card16
            };

            //   playerScores = new List<ListBoxItem> { P1Item, P2Item, P3Item, P4Item };

            playerNames = new List<string> { "Player1", "Player2", "Player3", "Player4" };
        }

       /**
       * Method:   JoinPlayer_Click - onClick method for all join buttons
       * @param    object sender (Button) and RoutedEventArgs e
       * @return   void
       */
        private void JoinPlayer_Click(object sender, RoutedEventArgs e)
        {
            string player = ((Button)sender).Name;
            try
            {
                playerName = player;
                if (connectToGameServer())
                {
                    Player1.Visibility = Visibility.Hidden;
                    Player2.Visibility = Visibility.Hidden;
                    Player3.Visibility = Visibility.Hidden;
                    Player4.Visibility = Visibility.Hidden;

                    switch (player)
                    {
                        case "Player1":
                            Player1.Visibility = Visibility.Visible;
                            Player1.IsEnabled = false;
                            break;
                        case "Player2":
                            Player2.Visibility = Visibility.Visible;
                            Player2.IsEnabled = false;
                            break;
                        case "Player3":
                            Player3.Visibility = Visibility.Visible;
                            Player3.IsEnabled = false;
                            break;
                        case "Player4":
                            Player4.Visibility = Visibility.Visible;
                            Player4.IsEnabled = false;
                            break;
                    }
                    btnstartGame.IsEnabled = true;
                    whoisthere();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private async void whoisthere()
        {
            await Task.Delay(1111);
            gameServer.Whoisthere();
        }

        private void updateStartGame()
        {
            if (gameboard.player1Joined || gameboard.player2Joined || gameboard.player3Joined || gameboard.player4Joined)
                btnstartGame.IsEnabled = true;
        }

       /**
       * Method:   StartGame_Click - onClick method for start button
       * @param    object sender (Button) and RoutedEventArgs e
       * @return   void
       */
        private void StartGame_Click(object sender, RoutedEventArgs e)
        {
            gameServer.Prepared(playerName);
            btnstartGame.IsEnabled = false;
            btnstartGame.Background = Brushes.DarkGray;

        }

       /**
       * Method:   StartGame - updates GUI on server call to start game
       * @param    string[] commandset - contains card order values
       * @return   void
       */
        private void StartGame(string[] commandset)
        {
            InitializeGameBoard(commandset);
            //   gameServer.InitializeClient();

            // enable all card buttons
            foreach (var card in cards)
                card.IsEnabled = true;

            // hide all images
            for (int i = 0; i < 16; i++)
            {
                Image img = findImgByNum(i);
                img.Source = new BitmapImage(back);
            }
        }

       /**
       * Method:   CardClicked - onClick method for all card buttons
       * @param    object sender (Button) and RoutedEventArgs e
       * @return   void
       */
        private void CardClicked(object sender, RoutedEventArgs e)
        {
            if (playerName == currentPlayer && clickCredit > 0)
            {
                string cardname = ((Button)sender).Name;
                cardname.Remove(0, 4);
                string cardPosition = cardname.Remove(0, 4);

                if (!cardList.Where(c => c.position == cardPosition).First().Disabled)
                {
                    if (clickCredit == 2)
                    {
                        gameServer.TryFirstCard(cardPosition);
                        c1 = cardname;
                        //take credit
                        clickCredit--;
                    }
                    else if (clickCredit == 1 && cardname != c1)
                    {
                        gameServer.Try2ndCard(cardPosition);
                        //take credit
                        clickCredit--;
                    }
                }
            }
        }

       /**
       * Method:   flip2Cards
       * @param    int num1 and num2 - represents card images that need to be flipped
       * @return   void
       */
        public async void flip2Cards(int num1, int num2)
        {
            Image img = findImgByNum(num1);
            img.Source = new BitmapImage(new Uri(gameboard.flipCard(cardList.ElementAt(num1 - 1), img), UriKind.Relative));
            Image img2 = findImgByNum(num2);
            img2.Source = new BitmapImage(new Uri(gameboard.flipCard(cardList.ElementAt(num2 - 1), img2), UriKind.Relative));

            await Task.Delay(1111);
            img.Source = new BitmapImage(back);
            img2.Source = new BitmapImage(back);

            if (playerName == currentPlayer)
                gameServer.TurnIsDone(playerName + " flip 2 cards");
        }

        private void showCard(string numstr)
        {
            int num = int.Parse(numstr);
            Image img = findImgByNum(num);
            img.Source = new BitmapImage(new Uri(gameboard.flipCard(cardList.ElementAt(num - 1), img), UriKind.Relative));
        }

       /**
       * Method:   showCard2 - shows the second card (i.e. a second flip)
       * @param    string numstr - contains card to be flipped
       * @return   void
       */
        private void showCard2(string numstr)
        {
            showCard(numstr);
            if (playerName == currentPlayer)
                gameServer.TurnIsDone(playerName + " show 2nd card");
        }

        /**
        * Method:   InitializeGameBoard - sets the initial game board state
        * @param    string cardOrder - contains card path for Card Front
        * @return   void
        */
        public void InitializeGameBoard(string[] cardOrder)
        {
            cardList = new ObservableCollection<Card>();

            // create card objects
            for (int index = 1; index <= 16; ++index)
            {
                string img = cardOrder[index].Split(',')[1];
                // create card
                Card card = new Card();
                card.Front = "Images/" + img + ".jpg";
                card.Back = "Images/0.jpg";
                card.ActiveImage = card.Back;
                card.Disabled = false;
                card.isFlipped = false;
                card.position = index.ToString();
                cardList.Add(card);
            }
        }

        /**
        * Method:   gameover - displays winner(s), freezes card buttons and enables ready button
        * @param    string winners
        * @return   void
        */
        private void gameover(string winners)
        {
            // display winner(s)
            var winnersArray = winners.Split(' ');

            string msg = "Game over! The winner is: ";

            if (winnersArray.Length > 1)
            {

                for (int i = 0; i < winnersArray.Length; ++i)
                {
                    if (i == winnersArray.Length - 1)
                        msg += "and " + winnersArray[i] + "!";
                    else
                        msg += winnersArray[i] + ", ";
                }
                msg += " it's a tie.";
            }
            else
                msg += winners;

            tblPlayer.Text = msg;
            tblPlayer.Foreground = Brushes.DarkRed;

            MessageBox.Show(msg);

            // freeze all card buttons
            foreach (var card in cards)
            {
                card.IsEnabled = false;
            }

            // unfreeze ready button
            btnstartGame.IsEnabled = true;
            btnstartGame.Background = Brushes.Green;
        }

        // Helper methods
        Image findImgByNum(int num)
        {
            switch (num)
            {
                case 1:
                    return image1;
                case 2:
                    return image2;
                case 3:
                    return image3;
                case 4:
                    return image4;
                case 5:
                    return image5;
                case 6:
                    return image6;
                case 7:
                    return image7;
                case 8:
                    return image8;
                case 9:
                    return image9;
                case 10:
                    return image10;
                case 11:
                    return image11;
                case 12:
                    return image12;
                case 13:
                    return image13;
                case 14:
                    return image14;
                case 15:
                    return image15;
                case 16:
                    return image16;
            }

            return image1;
        }
        Button findCardByNum(int num)
        {
            switch (num)
            {
                case 1:
                    return card1;
                case 2:
                    return card2;
                case 3:
                    return card3;
                case 4:
                    return card4;
                case 5:
                    return card5;
                case 6:
                    return card6;
                case 7:
                    return card7;
                case 8:
                    return card8;
                case 9:
                    return card9;
                case 10:
                    return card10;
                case 11:
                    return card11;
                case 12:
                    return card12;
                case 13:
                    return card13;
                case 14:
                    return card14;
                case 15:
                    return card15;
                case 16:
                    return card16;
            }

            return card1;
        }

        private bool connectToGameServer()
        {
            try
            {
                // Configure the ABCs of using the MessageBoard service
                DuplexChannelFactory<IGameBoard> channel = new DuplexChannelFactory<IGameBoard>(this, "MessagingService");

                // Activate a MessageBoard object
                gameServer = channel.CreateChannel();

                int result = gameServer.Join(playerName);
                if (result == 0)
                    return true;
                else if (result == -1)
                {
                    // Alias rejected by the service so nullify service proxies
                    gameServer = null;
                    MessageBox.Show("ERROR: Alias in use. Please try again.");
                    return false;
                }
                else if (result == -2)
                {
                    gameServer = null;
                    MessageBox.Show("ERROR:Game has Started!");
                    return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private delegate void ClientUpdateDelegate(string[] messages);
        public void UpdateClientObsolete(string[] dataset)
        {
            if (this.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                try
                {
                    //receive message from the server
                    //******************
                    var DataToProcess = dataset;

                    foreach (var command in DataToProcess)
                    {
                        var commandSet = command.Split(',');
                        switch (commandSet[0])
                        {
                            case "match":
                                //someoneFoundMatch = true;

                                showCard(commandSet[1]);
                                showCard(commandSet[2]);
                                if (currentPlayer == playerName)
                                {
                                    clickCredit = 2;
                                }
                                break;
                            case "switch":
                                {
                                    currentPlayer = commandSet[1];

                                    if (currentPlayer == playerName)
                                        clickCredit = 2;

                                    //display CURRENT PLAYER

                                }
                                break;
                            case "flip1":
                                flip(int.Parse(commandSet[1]));
                                break;
                            case "flip2":
                                flip2(int.Parse(commandSet[1]));
                                break;
                            case "show":
                                showCard(commandSet[1]);
                                break;
                            case "over":
                                //gameoverfunction();
                                break;
                            case "start":
                                //startgamefunction();
                                break;
                        }
                    }


                    {

                        //process the data

                        if (DataToProcess.Length == 4)
                        {
                            //use the data to change the status of the app
                            //0.maybe not enable the board
                            //1. current player
                            //2. pictures to show
                            //3. refresh score board 
                            //4. if the game is end 

                        }
                        else if (DataToProcess.Length == 2)
                        {
                            //maybenot  0. enable the board 
                            //1. current player         
                        }

                    }


                    //        LIST_Pictures.ItemsSource = DataToProcess;



                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                this.Dispatcher.BeginInvoke(new ClientUpdateDelegate(UpdateClientObsolete), new object[] { dataset });
        }



        public void UpdateClient(CallbackInfo info)
        {
            string[] dataset = info.Detail;
            if (this.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                try
                {
                    var DataToProcess = dataset;

                    foreach (var command in DataToProcess)
                    {
                        var commandSet = command.Split(',');
                        switch (commandSet[0])
                        {
                            case "match":
                                // someoneFoundMatch = true;
                                showCard(commandSet[1]);
                                showCard2(commandSet[2]);
                                foreach (var c in cardList)
                                {
                                    if (c.position == commandSet[1] || c.position == commandSet[2])
                                    {
                                        c.Disabled = true;
                                        // disable buttons of matched cards
                                        findCardByNum(int.Parse(commandSet[1])).IsEnabled = false;
                                        findCardByNum(int.Parse(commandSet[2])).IsEnabled = false;
                                    }
                                }

                                if (currentPlayer == playerName)
                                {
                                    clickCredit = 2;
                                }
                                break;
                            case "switch":
                                {
                                    currentPlayer = commandSet[1];

                                    if (currentPlayer == playerName)
                                    {
                                        clickCredit = 2;
                                        //display CURRENT PLAYER
                                        tblPlayer.Text = "Your Turn";
                                        tblPlayer.Foreground = Brushes.DarkGreen;
                                    }
                                    else
                                    {
                                        //display CURRENT PLAYER
                                        tblPlayer.Text = currentPlayer + " is playing...";
                                        tblPlayer.Foreground = Brushes.Blue;
                                    }
                                }
                                break;
                            case "flip2":
                                flip2Cards(int.Parse(commandSet[1]), int.Parse(commandSet[2]));
                                break;
                            case "show":
                                showCard(commandSet[1]);
                                break;
                            case "gameover":
                                gameover(commandSet[1]);
                                break;
                            case "startgame":
                                StartGame(dataset);
                                break;
                            case "scoreboard":
                                updateScoreBoard(commandSet);
                                break;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                this.Dispatcher.BeginInvoke(new ClientUpdateDelegate(UpdateClientObsolete), new object[] { dataset });
        }

        /**
        * Method:   updateScoreBoard
        * @param    string[] commandSet - contains player names and their scores
        * @return   void
        */
        void updateScoreBoard(string[] commandSet)
        {
            string str = "";
            List<string> ls = new List<string>();
            for (int i = 1; i < commandSet.Length; i++)
            {
                if (i % 2 != 0)
                {
                    str = commandSet[i];
                }
                else
                {
                    try
                    {
                        int.Parse(commandSet[i]);
                        str += " : " + commandSet[i];
                        ls.Add(str);
                    }
                    catch (Exception)
                    {
                        str += "" + commandSet[i];
                        ls.Add(str);
                    }
                }
            }

            lbScore.ItemsSource = ls;

        }

        public async void flip(int num1)
        {
            Image img = findImgByNum(num1);
            img.Source = new BitmapImage(new Uri(gameboard.flipCard(cardList.ElementAt(num1 - 1), img), UriKind.Relative));
            await Task.Delay(1111);
            img.Source = new BitmapImage(back);
        }

        public async void flip2(int num2)
        {
            Image img2 = findImgByNum(num2);
            img2.Source = new BitmapImage(new Uri(gameboard.flipCard(cardList.ElementAt(num2 - 1), img2), UriKind.Relative));
            await Task.Delay(1111);
            img2.Source = new BitmapImage(back);
            if (playerName == currentPlayer)
                gameServer.TurnIsDone(playerName + " flip 2 cards");
        }

        private void Leave(object sender, EventArgs e)
        {
            gameServer.Leave(playerName);
        }
    }
}
