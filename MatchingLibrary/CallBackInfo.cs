﻿/*
 * Program:         MatchingLibrary 
 * Module:          CallbackInfo.cs
 * Date:            April 2nd, 2019
 * Author:          Jinzhi Yang 
 * Description:     The CallbackInfo class represents a WCF data contract for sending
 *                  realtime updates to connected clients regarding changes to the 
 *                  state of the Gameboard (service object).
 *                  
 *                  Name - name of command sent (flip, match, show, gameover etc.)
 *                  Details - information needed to process command (cards to be flipped, player(s) who won etc.)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization; // WCF DataContract types

namespace MatchingLibrary
{
    [DataContract]
    public class CallbackInfo
    {
        [DataMember] public string Name { get; private set; } 

        [DataMember] public string [] Detail { get; private set; }


        public CallbackInfo(string name, List<string> detail)
        {
            Name = name;
            Detail = detail.ToArray();
        }
    }
}
