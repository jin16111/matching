﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using System.ServiceModel;  // WCF types

namespace MatchingLibrary
{


    public interface ICallback
    {
        [OperationContract(IsOneWay = true)]
        void UpdateClientObsolete(string[] messages);

        [OperationContract(IsOneWay = true)]
        void UpdateClient(CallbackInfo info);

    }




    [ServiceContract(CallbackContract = typeof(ICallback))]
    public interface IGameBoard
    {
        [OperationContract]
        int Join(string name);

        [OperationContract(IsOneWay = true)]
        void Leave(string name);

        [OperationContract(IsOneWay = true)]
        void ClickCard(string message);
        [OperationContract(IsOneWay = true)]
        void TryFirstCard(string message);
        [OperationContract(IsOneWay = true)]
        void Try2ndCard(string message);
        [OperationContract(IsOneWay = true)]
        void TurnIsDone(string who);
        [OperationContract(IsOneWay = true)]
        void Prepared(string playername);

        [OperationContract(IsOneWay = true)]
        void Whoisthere();


        //[OperationContract]
        //string[] InitializeClient(); 

    }


    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class GameBoard : IGameBoard
    {
        private Dictionary<string, ICallback> callbacks = new Dictionary<string, ICallback>();
        private Dictionary<string, int> scoreBoard = new Dictionary<string, int>();
        public string currentPlayer = "Player0";
        private bool gameInitialized = false;
        private List<string> cardOrder = new List<string>();
        private bool gameIsOver = false;
        private int numPrepared = 0;


        List<string> nums;
        List<Card> cards;

        public string Card1Position = "";
        public string Card2Position = "";
        public string Card1Value = "";
        public string Card2Value = "";

        public List<string> solvedCards = new List<string>();
        private List<string> messages = new List<string>();

        public bool player1Joined = false;
        public bool player2Joined = false;
        public bool player3Joined = false;
        public bool player4Joined = false;

        public bool startgame = false;


        // public ObservableCollection<Card> cardList;
        public Card selectedCard1, selectedCard2;
        public Image selectedImage1, selectedImage2;
        Uri back = new Uri("Images/0.jpg", UriKind.Relative);


        void IGameBoard.Prepared(string playername)
        {
            numPrepared++;
            if (numPrepared == callbacks.Count)
                startGame();
        }

        private void startGame()
        {
            currentPlayer = "Player0";
            ShiftTurn();
            InitializeGameBoard();
            updateScoreClientBoard();
            List<string> dataset = new List<string>();
            //flip card 2
            dataset.Add("startgame");
            cardOrder.ForEach(combo => dataset.Add(combo));
            CallbackInfo info = new CallbackInfo(dataset[0], dataset);
            sendDataToClient(info);
        }

        public void InitializeGameBoard()
        {
            Random rng = new Random();

            nums = new List<string>();

            for (int i = 1; i <= 8; i++)
            {
                nums.Add(i.ToString());
            }
            for (int i = 1; i <= 8; i++)
            {
                nums.Add(i.ToString());
            }

            // shuffle cards in random order
            int n = nums.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                string value = nums[k];
                nums[k] = nums[n];
                nums[n] = value;
            }




            cardOrder = new List<string>();
            for (int i = 1; i <= 16; i++)
            {
                cardOrder.Add(i + "," + nums[i - 1]);
            }
            cardOrder.ForEach(combo => Console.WriteLine(combo + " "));

            gameInitialized = true;
        }

        // Previous version - wanted to test
        public void InitializeGameBoard2()
        {
            Random rng = new Random();
            cards = new List<Card>();

            // create card objects
            for (int i = 0; i < 8; ++i)
            {
                // create card
                Card card = new Card();
                card.Front = "Images/" + (i + 1) + ".jpg";
                card.Back = "Images/0.jpg";
                card.ActiveImage = card.Back;
                card.Disabled = true;
                card.isFlipped = false;

                // create copy
                Card card2 = new Card();
                card2.Front = "Images/" + (i + 1) + ".jpg";
                card2.Back = "Images/0.jpg";
                card2.ActiveImage = card2.Back;
                card2.Disabled = true;
                card2.isFlipped = false;
                cards.Add(card);
                cards.Add(card2);
            }

            // shuffle cards in random order
            int n = cards.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Card value = cards[k];
                cards[k] = cards[n];
                cards[n] = value;
            }
        }

        public string flipCard(Card card, Image image)
        {
            card.isFlipped = true;
            card.ActiveImage = card.Front;

            // check if a card has not been selected
            if (selectedCard1 == null)
            {
                selectedCard1 = card;
                selectedImage1 = image;
            }
            // check if a second card has not been selected
            else if (selectedCard2 == null)
            {
                selectedCard2 = card;
                selectedImage2 = image;
            }



            return card.ActiveImage;
        }

        public int Join(string name)
        {
            if (callbacks.ContainsKey(name))
                // User alias must be unique
                return -1;
            else if (gameInitialized)
                return -2;
            else
            {
                // Retrieve client's callback proxy
                ICallback cb = OperationContext.Current.GetCallbackChannel<ICallback>();
                // Save alias and callback proxy
                callbacks.Add(name, cb);
                scoreBoard.Add(name, 0);
                Console.WriteLine(name + "joined");
                return 0;
            }
        }

        public void Leave(string name)
        {
            if (callbacks.ContainsKey(name))
            {
                callbacks.Remove(name);

                if (currentPlayer == name)
                    ShiftTurn();
                if (!gameInitialized)
                    scoreBoard.Remove(name); 
                updateScoreClientBoard();
            }
        }

        public void TryFirstCard(string Position)
        {
            Console.WriteLine("received card1:" + Position);
            //sign position value
            Card1Position = Position;

            //then flip it 
            List<string> dataset = new List<string>();
            dataset.Add("show," + Card1Position);
            CallbackInfo info = new CallbackInfo(dataset[0], dataset);
            sendDataToClient(info);
        }

        public void Try2ndCard(string Position)
        {
            Console.WriteLine("received card2:" + Position);
            //sign the value
            Card2Position = Position;

            //compare with card1
            string matchedCardValue = checkMatch();
            if (matchedCardValue != "")
            {
                List<string> dataset = new List<string>();
                // 1. update score board  
                scoreBoard[currentPlayer]++;

                // 2. update score board msg 
                updateScoreClientBoard();

                // 3. add the match num to solvedcards
                solvedCards.Add(matchedCardValue);
                Console.Write("solved =" + solvedCards.Count + ": ");
                solvedCards.ForEach(c => Console.WriteLine(c + ","));

                // 4. let user show the cards: Card1Position, Card2Position  
                dataset.Add("match," + Card1Position + "," + Card2Position);
                CallbackInfo info = new CallbackInfo(dataset[0], dataset);
                sendDataToClient(info);
            }
            else
            {
                Console.WriteLine("flip card2:" + Position);
                List<string> dataset = new List<string>();
                //flip card 2
                dataset.Add("flip2," + Card1Position + "," + Card2Position);
                CallbackInfo info = new CallbackInfo(dataset[0], dataset);
                sendDataToClient(info);
            }
        }

        private void updateScoreClientBoard()
        {
            List<string> dataset = new List<string>();

            string str = "";
            foreach (var item in scoreBoard)
            {
                if (!gameInitialized)
                {
                    str += "," + item.Key + "," + " is Joined";
                }
                else
                {
                    if (callbacks.ContainsKey(item.Key))
                        str += "," + item.Key + "," + item.Value;
                    else
                        str += "," + item.Key + "(offline)," + item.Value;
                }
            }
            Console.WriteLine("scoreboard" + str);
            dataset.Add("scoreboard" + str);

            CallbackInfo info = new CallbackInfo(dataset[0], dataset);
            sendDataToClient(info);
        }

        public void TurnIsDone(string who)
        {
            Console.WriteLine("----------turn is done ----" + who + "-------");
            ShiftTurn();
            //reset 
            Card1Position = "";
            Card2Position = "";

            //update score board

            //check if gameover
            if (solvedCards.Count == 8)
                gameover();
        }

        private void ShiftTurn()
        {
            List<string> dataset = new List<string>();
            //shiftTurn
            int newPlayenum = 0;
            newPlayenum = int.Parse(currentPlayer[6].ToString());
            do
            {
                newPlayenum += 1;
                if (newPlayenum > 4)
                    newPlayenum = 1;
            } while (!callbacks.ContainsKey("Player" + newPlayenum));
            currentPlayer = "Player" + newPlayenum;
            dataset.Add("switch," + currentPlayer);
            //sendDataToClientObsolete(dataset);
            CallbackInfo info = new CallbackInfo(dataset[0], dataset);
            sendDataToClient(info);
            Console.WriteLine("shift Player to " + currentPlayer);
        }

        private void gameover()
        {
            List<string> dataset = new List<string>();

            int highestScore = 0;
            string winner = "";
            foreach (var score in scoreBoard)
            {
                // win
                if (score.Value > highestScore)
                {
                    highestScore = score.Value;
                    winner = score.Key;
                }
                // tie
                else if (score.Value == highestScore)
                    winner += " " + score.Key;
            }




            dataset.Add("gameover," + winner);
            CallbackInfo info = new CallbackInfo(dataset[0], dataset);
            sendDataToClient(info);
            Console.WriteLine("gameover winner =  " + winner);

            gameIsOver = true;
            gameInitialized = false;
            numPrepared = 0;
            solvedCards.Clear();
            //clean Scorebord
            scoreBoard.Clear(); 
            foreach (var item in callbacks)
            { 
                    scoreBoard[item.Key]=0;
            }
        }

        public void ClickCard(string Position)
        {
            List<string> dataset = new List<string>();

            if (Card1Position == "")
            {
                Card1Position = Position;
                Card1Value = findCardValue(Position);


                Console.WriteLine("flip card I: " + Card1Position + "/" + Card1Value + "");
                dataset.Add("flip1," + Card1Position);
                CallbackInfo info = new CallbackInfo(dataset[0], dataset);
                sendDataToClient(info);
            }
            else
            {
                Card2Position = Position;
                Card2Value = findCardValue(Position);

                Console.WriteLine("Show card II: " + Card2Position + "/" + Card2Value + "");
                dataset.Add("show," + Card2Position);
            }

            if (Card1Position != "" && Card2Position != "")
            {

                string matchedCardFront = checkMatch();

                //see if match
                if (matchedCardFront != "")
                {
                    // 1. update score board 
                    // 2. update score board msg

                    // 3. add the match num to solvedcards
                    solvedCards.Add(matchedCardFront);
                    // 4. let user show the cards: selectedCard1num, selectedCard1num 
                    dataset.Add("match," + Card1Position + "," + Card2Position);

                    //sendDataToClientObsolete(dataset);
                    CallbackInfo info1 = new CallbackInfo(dataset[0], dataset);
                    sendDataToClient(info1);

                    if (solvedCards.Count == 8)
                    {
                        //game over
                        dataset.Add("over,");

                        //reset game
                        gameInitialized = false;

                    }
                }
                else //here flip the 2nd card
                {
                    dataset.Add("flip2," + Card2Position);
                    CallbackInfo info1 = new CallbackInfo(dataset[0], dataset);
                    sendDataToClient(info1);
                    Console.WriteLine("flip card II: " + Card2Position + "/" + Card2Value + "");
                }


                //shiftTurn
                int newPlayenum = 0;
                newPlayenum = int.Parse(currentPlayer[6].ToString());
                do
                {
                    newPlayenum += 1;
                    if (newPlayenum > 4)
                        newPlayenum = 1;
                } while (!callbacks.ContainsKey("Player" + newPlayenum));


                currentPlayer = "Player" + newPlayenum;
                dataset.Add("switch," + currentPlayer);

                Console.WriteLine("currentPlayer: " + currentPlayer);

                //sendDataToClientObsolete(dataset);
                CallbackInfo info = new CallbackInfo(dataset[0], dataset);
                sendDataToClient(info);

                //reset 
                Card1Position = "";
                Card2Position = "";
                //displayn1n2();
            }

        }

        public string[] InitializeClient()
        {
            if (!gameInitialized)
            {
                Console.WriteLine("initializing game");
                InitializeGameBoard();
                gameInitialized = true;
            }
            return cardOrder.ToArray();
        }

        private string checkMatch()
        {
            Card1Value = findCardValue(Card1Position);
            Card2Value = findCardValue(Card2Position);
            if (Card1Value == Card2Value)
            {
                return Card1Value;
            }
            else
                return "";
        }


        private string findCardValue(string cardnum)
        {
            foreach (var item in cardOrder)
            {
                var pair = item.Split(',');
                if (pair[0] == cardnum)
                {
                    return pair[1];
                }
            }
            return "";
        }

        //helper
        private void sendDataToClientObsolete(List<string> dataset)
        {
            String[] msgs = dataset.ToArray<string>();

            foreach (ICallback cb in callbacks.Values)
                cb.UpdateClientObsolete(msgs);
        }

        private void sendDataToClient(CallbackInfo info)
        {
            foreach (ICallback cb in callbacks.Values)
                cb.UpdateClient(info);
        }

        public void Whoisthere()
        {
            updateScoreClientBoard();
        }
    }
}
