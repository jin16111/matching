﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchingLibrary
{
    public class Card : INotifyPropertyChanged
    {
        public string position { get;set; }
        string _activeImage; 
        public string Front { get; set; }
        public string Back { get; set; }
        public string ActiveImage
        {
            get { return _activeImage; }

            set
            {
                if (_activeImage != value)
                {
                    _activeImage = value;
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("ActiveImage"));
                }
            }
        }
        public bool Disabled { get; set; }
        public bool isFlipped { get; set; }
        

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
